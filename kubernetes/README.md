# Setup Master 
- Do not create swapfile, if you have a swapfile, disable it in hardware-configuration.nix then rebuild with
`nixos-rebuild switch`. 

- copy the master-node.nix file to /etc/nixos/configuration.nix then `nixos-rebuild switch`.
