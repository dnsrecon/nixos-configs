{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];


  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # this was necesarry on install
  # boot.loader.grub.forceInstall = true;
  boot.loader.grub.device = "/dev/vda";
  boot.loader.grub.extraConfig = "
      serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1
      terminal_input serial console
      terminal_output serial console
  ";
  boot.loader.systemd-boot.enable = true;

  boot.kernelParams = [
                    "console=ttyS0,115200n8"
                    "ip=208.79.92.66::208.79.92.65:255.255.255.252:gw:eth0"
  ];


  boot.kernel.sysctl = {
  "net.core.rmem_default" = 16777216;
  "net.core.rmem_max" = 16777216;
  "net.core.wmem_default"  = 16777216;
  "net.core.wmem_max" = 4194304;
  "net.ipv4.conf.all.forwarding" = 1;
  "net.ipv4.conf.all.log_martians" = 1;
  "net.ipv4.conf.all.mc_forwarding" = 1;
  "net.ipv4.conf.default.log_martians" = 1;
  "net.ipv4.ip_forward_use_pmtu" = 1;
  "net.ipv4.netfilter.ip_conntrack_tcp_timeout_established" = 86400;
  "net.ipv4.tcp_dsack" = 0;
  "net.ipv4.tcp_fack" = 0;
  "net.ipv4.tcp_mem" = "1638400 1638400 1638400";
  "net.ipv4.tcp_rmem" = "4096 87380 4194304";
  "net.ipv4.tcp_sack" = 0;
  "net.ipv4.tcp_slow_start_after_idle" = 0;
  "net.ipv4.tcp_syncookies" = 1;
  "net.ipv4.tcp_wmem" = "4096 87380 4194304";
  "net.ipv4.tcp_timestamps" = 0;
  "net.ipv6.conf.all.forwarding" = 1;
  "net.ipv6.conf.all.mc_forwarding" = 1;
  "net.ipv6.conf.eth0.accept_ra" = 1;
  "net.netfilter.nf_conntrack_max" = 196608;
  };

  boot.cleanTmpDir = true;
  boot.initrd.network.enable = true;
  boot.initrd.network.ssh.enable = true;
  boot.initrd.network.ssh.authorizedKeys = [" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPLFjUT/f/LxaFBDe1WGgpEM8BBAMfif2HWEA9czptDz erratic@purple" ];

  system.autoUpgrade.enable = true;
  #services.journalbeat.enable
  networking.dhcpcd.enable = false;

  networking.vswitches.EXT.interfaces = [ "SEATTLE1" ];
  networking.vswitches.INT.interfaces = [];

  networking.interfaces.EXT.ipv4.addresses = [
  { address = "206.125.168.65"; prefixLength = 28; }];

  networking.interfaces.EXT.ipv6.addresses = [
  { address = "2607:f2f8:a2c4:1337:0:0:0:0"; prefixLength = 64; }];

  networking.interfaces.INT.ipv4.addresses = [
  { address = "10.242.128.1"; prefixLength = 24; }];

  networking.interfaces.INT.ipv6.addresses = [
  { address = "2607:f2f8:a2c4:255:0:0:0:0"; prefixLength = 64; }];

  networking.hostName = "gw";
  networking.domain = "sylmar.ca.us.yourstruly.sx";
  networking.nameservers = [ "127.0.0.1" ];

  networking.firewall.autoLoadConntrackHelpers = true;
  networking.firewall.logRefusedConnections = true;
  networking.firewall.logRefusedPackets = true;
  networking.firewall.logReversePathDrops = true;
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [22 53];
  networking.firewall.allowedUDPPorts = [22 53];

  networking.extraHosts =
  ''
    208.79.92.66 gw.sylmar.ca.us.yourstruly.sx gw
  '';

  i18n = {
     consoleFont = "Lat2-Terminus16";
     consoleKeyMap = "us";
     defaultLocale = "en_US.UTF-8";
   };

   time.timeZone = "UTC";

   environment.systemPackages = with pkgs; [ wget ispell vim curl emacs git jq ethtool cri-tools socat ebtables tcpdump git easyrsa];
   virtualisation.vswitch.resetOnStart = true;
   virtualisation.docker.enable = true;
   virtualisation.docker.logDriver = "journald";
   virtualisation.docker.enableOnBoot = true;
   virtualisation.docker.storageDriver = "btrfs";
   virtualisation.docker.autoPrune.enable = true;
   virtualisation.vswitch.enable = true;

   services.dhcpd4.enable = true;
   services.dhcpd4.interfaces = [ "INT" "EXT" ];
   services.dhcpd4.extraConfig =
   ''
     subnet 10.242.128.0 netmask 255.255.255.0 {
         authoritative;
         range 10.242.128.2 10.242.128.254;
     }

     subnet 206.125.168.64 netmask 255.255.255.240 {
         authoritative;
         range 206.125.168.66 206.125.168.78;
     }
   '';

   services.dhcpd6.enable = true;
   services.dhcpd6.interfaces = [ "INT" "EXT" ];
   services.dhcpd6.extraConfig =
   ''
     subnet6 2607:f2f8:a2c4:1337::/64 {
         authoritative;
     	   range6 2607:f2f8:a2c4:1337:0:0:0:0 2607:f2f8:a2c4:1337:ffff:ffff:ffff:ffff;
     }
   '';

   services.powerdns.enable = true;
   services.powerdns.extraConfig = ''
     allow-axfr-ips=127.0.0.1 ::1 206.125.168.64/28 2607:f2f8:a2c4:255:0:0:0::/64;
     daemon=yes
     disable-axfr=no
     guardian=yes
     local-address=0.0.0.0
     local-ipv6=::
     local-port=5353
     loglevel=4
     master=yes
     query-local-address=127.0.0.1
     query-local-address6=::1
     webserver=yes
     webserver-address=127.0.0.1
     webserver-password=
     webserver-port=8081
     version-string=netcrave
     launch=gmysql
     loglevel=10
     log-dns-queries=1
     gmysql-host=127.0.0.1
     gmysql-user=root
     gmysql-password=
     gmysql-dbname=powerdns
   '';

   services.pdns-recursor.enable = true;
   services.pdns-recursor.dns.port = 53;
   services.pdns-recursor.dns.allowFrom = ["10.242.0.0/16" "127.0.0.1" "206.125.168.64/28" "2607:f2f8:a2c4::/48"];
   services.pdns-recursor.dnssecValidation = "validate";
   services.pdns-recursor.extraConfig =
   ''
     forward-zones=yourstruly.sx=127.0.0.1:5353
   '';

   users.users.root.openssh.authorizedKeys.keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDhV4X1qsnmSUI5uvGuDIRpehlr9n91iMoHXOik0TO/vU8LECnMBITdlnFjsKwQYLaQIlboUe3nKqu69CKtFXVoqXUTLxYAsghlbrnMSW1/bQgAdM/qbucak7V0gFvLNSfQ8QYsTmeoOO3DbzoJh5ifqh4KnmWbeidjJqKfq/5aRIYTLUhmUHF3UpefQc0nxUBi+m8cj40vJhwCARXkH8cKCIomgxCl+75Tj6oAFuBByOEp+/VWptS6sdGc4Qk33spn4JV/f2Somy/ccGGqsTmpLr4yop7y55qF2OEB/xIRF9cWz74uesBCYYKoJOGo3TNQSMUbhb9cnOQ6EuKURmLDJXm4bvRgLjjN7A04RKJ1ffHyuuMEl919NgrqqorbpZZNF5V3JVxFNyQHifEp2XdCLifFWV04FTvfRnCRG/57dZjkeCTqYskIONFYkRjP37wpbbreZHd1OmAqnrEYtunVinNy4vX0v6flbvmyjfnUOCbJMt12rmm4JA5Lq+JY5BOyDKEX2mcyqgel0zW40edxHOuUXpA4z/8PMrzTWnBj1nWXwt6wbCwakH+A1dKMRvcZPL25D34JdjVZdSP2BvMNgG9GpcWv5XfpUzZUSqe3IDMPR2ATwuq8+FwxSrzMWEP2kA/Y9sgS4tz5yq4w9GZ+9tiF+XOiJlq0k+fYgaqqLQ== erratic@molly"];
   services.openssh.enable = true;
   services.openssh.permitRootLogin = "yes";
   services.openssh.passwordAuthentication = false;
   services.openssh.challengeResponseAuthentication = false;

   services.mysql.package = pkgs.mariadb;
   services.mysql.enable = true;

   services.mysql.ensureDatabases = ["powerdns"];
   services.mysql.rootPassword = "";
   services.mysql.user = "mysql";

   services.openvpn.servers.SEATTLE1_WA_US_YOURSTRULY.autoStart = true;
   services.openvpn.servers.SEATTLE1_WA_US_YOURSTRULY.config =
   ''
   mode server
   tls-server
   local gw.sylmar.ca.us.yourstruly.sx
   port 1194
   dev SEATTLE1
   dev-type tap
   persist-key
   persist-tun
   pkcs12 /etc/ssl/gw.sylmar.ca.us.yourstruly.sx.p12
   askpass /etc/ssl/password
   client-to-client
   user nobody
   keepalive 2 4
   verb 3
   cipher AES-256-GCM
   auth SHA512
   tls-version-min 1.2
   tls-cipher TLS-ECDHE-ECDSA-WITH-AES-256-GCM-SHA384
   dh none
   ecdh-curve secp521r1
   sndbuf 393216
   rcvbuf 393216
   push "sndbuf 393216"
   push "rcvbuf 393216"
   '';
}
