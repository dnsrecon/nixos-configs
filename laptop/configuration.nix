# TODO
# figure out how to use params
# https://github.com/andir/nixops-dn42/blob/caaa02bcf122f193af3a7e936d733d6c6a8d9a10/boolean.nix

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";
  boot.loader.efi.canTouchEfiVariables = true;

  boot.supportedFilesystems = ["zfs"];
  boot.kernelParams = ["console=ttyS0,115200n8" "console=tty0"];

  boot.kernel.sysctl = {
     "net.netfilter.nf_conntrack_max" = 196608;
     "net.ipv4.netfilter.ip_conntrack_tcp_timeout_established" = 86400;
     "net.core.wmem_max" = 4194304;
     "net.core.rmem_max" = 12582912;
     "net.ipv4.tcp_rmem" = "4096 87380 4194304";
     "net.ipv4.tcp_wmem" = "4096 87380 4194304";
     "net.ipv4.conf.all.log_martians" = 1; 
     "net.ipv4.conf.default.log_martians" = 1;
     "net.ipv4.tcp_syncookies" = 1;
  };

  networking.hostId = "FFFFFFFF";

  # networking.useNetworkd = true;

  networking.systemd.network = {
     enable = true;
     # These interfaces need to be provided by openvswitch
     netdevs."PUBLIC" {
        enable = true;
        # also netdevConfig https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/system/boot/networkd.nix#L208
        # vlanConfig https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/system/boot/networkd.nix#L220
        matchConfig = {
           Name = "PUBLIC";
        };
     };
     netdevs."PRIVATE" {
        enable = true;
        matchConfig = {
           Name = "PRIVATE";
        };
     };
  };

  networking.hostName = "purple"; # Define your hostname.
  networking.domain = "yourstruly.sx";
  networking.nameservers = ["1.1.1.1" "4.2.2.1" "8.8.8.8"];
  networking.networkmanager.enable = true;
  networking.firewall.enable = true;
  networking.firewall.allowPing = true;
  networking.extraHosts = 
  ''     
  '';

  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = false;
  hardware.pulseaudio.enable = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;
  hardware.sane.enable = true;
  hardware.cpu.intel.updateMicrocode = true;

  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioLight;
  }

  hardware.opengl.extraPackages = with pkgs; [
    # These are imported by ocl-icd
    intel-ocl  # intel cpu
    # beignet   # intel gpu
  ];

  sound.enable = true;

  security.sudo.enable = true;
  security.sudo.wheelNeedsPassword = false;

  i18n = {
     consoleFont = "Lat2-Terminus16";
     consoleKeyMap = "us";
     defaultLocale = "en_US.UTF-8";
  };
  
  time.timeZone = "UTC";

  powerManagement.enable = true;
  powerManagement.cpuFreqGovernor = "performance";
  
  nixpkgs.config.allowUnfree = true;
  # nix.binaryCaches = [ http://cache.nixos.org http://hydra.nixos.org ];

  environment.systemPackages = with pkgs; [
      wget vim emacs jq parallel chromium firefox gimp ntfs3g python python36 ruby nodejs php vscode go zsh git rxvt_unicode-with-plugins tmux weechat gmrun which cabal2nix cabal-install chromium dmenu ghc gnumake htop inconsolata keepassx keepassx2 keychain nix-prefetch-scripts nix-repl openconnect openconnect_openssl pass pciutils rxvt_unicode-with-plugins sqlite unzip wget which xorg.xbacklight xclip xorg.xev xautolock xsel parcellite xdotool bluez psmisc mc links2 w3m firefox-bin xcompmgr xfontsel xorg.xhost rxvt_unicode urxvt_perls stdenv androidndk atom pavucontrol mpc_cli (pkgs.ncmpcpp.override { outputsSupport = true; }) deadbeef ffmpeg youtube-dl mpv pkgs.python34Packages.livestreamer steam zathura hunspell hunspellDicts.en-us weechat libreoffice keepassx2 libnotify dunst irssi znc blender wireshark nmap masscan iftop tcpdump ethtool openssl whois bind ripgrep strace traceroute mtr iputils curlFull aria2 transmission unrar wine winetricks openvpn virtmanager virtmanager-qt
  ];

  nixpkgs.config.firefox = {
     enableAdobeFlash = false; # never
  };
    
  fonts.enableCoreFonts = true;
    fonts.fonts = with pkgs; [
       corefonts
       liberation_ttf
       dejavu_fonts
       terminus_font
       ubuntu_font_family
       gentium
       fira
       dejavu_fonts
       corefonts
       inconsolata
       lmodern
       terminus_font
       font-droid
  ];
  
  programs.zsh.ohMyZsh.enable = true;
  programs.zsh.ohMyZsh.plugins = [ "git" "bundler" "dotenv" "rake" "rbenv" "ruby" "man" "systemd" "sudo" "gpg-agent" "docker" "colorize" "gem" "npm" "go" "python" "helm"];
  programs.fish.enable = true;
  programs.bash.enableCompletion = true;

  programs.mtr.enable = true;

  programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
  };

  programs.ssh.extraConfig = 
  '' 
      VerifyHostKeyDNS yes
  ''; 

  programs.chromium = {
      enable = true;
      defaultSearchProviderSearchURL = "https://duckduckgo.com/?q={searchTerms}";
      defaultSearchProviderSuggestURL = null;
      extensions = [
        "ihlenndgcmojhcghmfjfneahoeklbjjh" # cVim
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # ublock origin
      ];
      extraOpts = {
        AlternateErrorPagesEnabled = false;
        AlwaysOpenPdfExternally = true;
        AutoFillEnabled = false;
        BackgroundModeEnabled = false;
        BlockThirdPartyCookies = true;
        BookmarkBarEnabled = false;
        BrowserAddPersonEnabled = false;
        BrowserGuestModeEnabled = false;
        BuiltInDnsClientEnabled = false;
        CloudPrintProxyEnabled = false;
        CloudPrintSubmitEnabled = false;
        ContextualSearchEnabled = false;
        DefaultBrowserSettingEnabled = false;
        DefaultGeolocationSetting = 2;
        DownloadDirectory = "/tmp";
        NTPContentSuggestionsEnabled = false;
        NetworkPredictionOptions = 2;
        PasswordManagerEnabled = false;
        SearchSuggestEnabled = false;
        SpellCheckServiceEnabled = false;
        TranslateEnabled = false;
      };
  };

  services.cron = {
     enable = true;
     systemCronJobs = [
         "@reboot ovs-vsctl addbr PUBLIC"
         "@reboot ovs-vsctl addbr PRIVATE"
     ];
  };

  # https://github.com/NixOS/nixpkgs/issues/18962
  # services.resolved.dnssec = "false";
  services.resolved.enable = true;
  services.timesyncd.enable = true;

  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";
  services.openssh.passwordAuthentication = false; 
  services.openssh.challengeResponseAuthentication = false;

  services.nixosManual.showManual = true;

  services.lldpd.enable = true;

  services.avahi = {
     enable = true;
     nssmdns = true;
  };

  services.printing.enable = true;

  services.zfs.autoScrub.enable = true;

  services.acpid.enable = true;

  services.xserver.enable = true;
  services.xserver.videoDrivers = [ "intel" "modsetting" ];
  services.xserver.layout = "us";
  services.xserver.libinput.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  

  services.gpm.enable = true;

  services.emacs.enable = true;
  services.emacs.install = true;
  services.emacs.defaultEditor = true;

#  services.tcsd.enable = true;
  
  services.teamviewer.enable = true; 

  services.redshift.enable = true;
  services.redshift.latitude = "47.6062";
  services.redshift.longitude = "122.3321";

  services.nfs.server = {
    enable = true;
    exports = ''
      /mnt/export/media 192.168.1.0/24(ro,subtree_check)
    '';
  };

  services.samba = {
    enable = true;
    extraConfig = ''
      [media]
        path = /mnt/export/media
        read only = yes
        guest ok = yes
      '' + (if config.services.transmission.enable then ''
        [torrents]
        path = /mnt/export/torrents
        read only = no
        guest ok = yes
        force user = transmission
      '' else "");
  };

  services.minidlna.enable = true;
  services.minidlna.mediaDirs = ["/mnt/export/media"];

  virtualisation.vswitch.enable = true;
  virtualisation.docker.enable = true;
  virtualisation.docker.storageDriver = "zfs";
  virtualisation.libvirtd.enable = true;
  virtualisation.libvirtd.qemuOvmf = true;

  users.extraUsers.erratic = {
     isNormalUser = true;
     uid = 1000;
     group = "users";
     extraGroups = [
        "wheel"
        "libvirtd"
        "docker" 
        "tape"
        "video"
        "audio"
        "dialout"
        "networkmanager"
     ];
  };

  users.users.root.openssh.authorizedKeys.keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDhV4X1qsnmSUI5uvGuDIRpehlr9n91iMoHXOik0TO/vU8LECnMBITdlnFjsKwQYLaQIlboUe3nKqu69CKtFXVoqXUTLxYAsghlbrnMSW1/bQgAdM/qbucak7V0gFvLNSfQ8QYsTmeoOO3DbzoJh5ifqh4KnmWbeidjJqKfq/5aRIYTLUhmUHF3UpefQc0nxUBi+m8cj40vJhwCARXkH8cKCIomgxCl+75Tj6oAFuBByOEp+/VWptS6sdGc4Qk33spn4JV/f2Somy/ccGGqsTmpLr4yop7y55qF2OEB/xIRF9cWz74uesBCYYKoJOGo3TNQSMUbhb9cnOQ6EuKURmLDJXm4bvRgLjjN7A04RKJ1ffHyuuMEl919NgrqqorbpZZNF5V3JVxFNyQHifEp2XdCLifFWV04FTvfRnCRG/57dZjkeCTqYskIONFYkRjP37wpbbreZHd1OmAqnrEYtunVinNy4vX0v6flbvmyjfnUOCbJMt12rmm4JA5Lq+JY5BOyDKEX2mcyqgel0zW40edxHOuUXpA4z/8PMrzTWnBj1nWXwt6wbCwakH+A1dKMRvcZPL25D34JdjVZdSP2BvMNgG9GpcWv5XfpUzZUSqe3IDMPR2ATwuq8+FwxSrzMWEP2kA/Y9sgS4tz5yq4w9GZ+9tiF+XOiJlq0k+fYgaqqLQ== erratic@molly"];

  system.stateVersion = "18.03";
  system.autoUpgrade.channel = https://nixos.org/channels/nixos-18.03;
}
