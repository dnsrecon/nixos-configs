# Installation instructions
boot up the livecd, it doesn't come with the tools you need to setup a ZFS filesytem so you have to install them on the livecd unionfs image. 

## Prepare the livecd / unionfs
This is possible because the image is mounted as a RAM disk, I know most livecds have this but the NixOS package manager is designed to be used this way Cd to /etc/nixos (on the livecd and add the following lines to it's config: 

```
{ config, pkgs, ... }:

{
  imports = [ <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-graphical-kde.nix> ];
  # add lines starting from here 
  boot.supportedFilesystems = ["zfs"];
}
```

then run nixos-rebuild switch. 

## Partition the disk 
You might notice I used ext2 for rpool and swap; that's fine I only did that because I'm not sure what the code is for ZFS and swap 
in parted and that get's changed/updated when the fileystem is created/formatted in the next step. As a side note, it's important 
to note that you need to use `set 1 bios_grub on` and not `set 1 boot on` such as is required by the systemd bootloader; grub uses a different partition flag and is necesarry to boot from a ZFS partition. It's also important to note that switching between bios_grub and esp will corrupt the partition and the filesystem as well. So don't ever do that, just use grub:
```
[root@nixos:/etc/nixos]# parted /dev/sda
GNU Parted 3.2
Using /dev/sda
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) unit mib
(parted) mklabel gpt
(parted) mkpart
Partition name?  []? boot
File system type?  [ext2]? fat32
Start? 1
End? 100
(parted) mkpart
Partition name?  []? swap
File system type?  [ext2]?
Start? 100
End? 4096
(parted) mkpart
Partition name?  []? rpool
File system type?  [ext2]?
Start? 4096
End? -1
(parted) set 1 bios_grub on
(parted) q
Information: You may need to update /etc/fstab.
```

## Create the filesystems
```
[root@nixos:/etc/nixos]# mkfs.vfat -F32 -n boot /dev/sda1
mkfs.fat 4.1 (2017-01-24)
mkfs.fat: warning - lowercase labels might not work properly with DOS or Windows

[root@nixos:/etc/nixos]# mkswap -L swap /dev/sda2
Setting up swapspace version 1, size = 3.9 GiB (4190105600 bytes)
LABEL=swap, UUID=9137bdcd-fd5d-4ce8-ac64-ae170ddffc6c
[root@nixos:/etc/nixos]# zpool create -o altroot=/mnt rpool /dev/sda3

[root@nixos:/etc/nixos]# zfs create -o mountpoint=none rpool/root

[root@nixos:/etc/nixos]# zfs create -o mountpoint=legacy rpool/root/nixos

[root@nixos:/etc/nixos]# zfs create -o mountpoint=legacy rpool/home

[root@nixos:/etc/nixos]#
```

### Compression 
If you want compression at this point is a good time to enable it: 
```
[root@nixos:/etc/nixos]# zfs set compression=lz4 rpool/root

[root@nixos:/etc/nixos]# zfs set compression=lz4 rpool/home
```

## Mount filesystems
```
[root@nixos:/etc/nixos]# mount -t zfs rpool/root/nixos /mnt

[root@nixos:/etc/nixos]# mkdir /mnt/{home,boot}

[root@nixos:/etc/nixos]# mount -t zfs rpool/home /mnt/home

[root@nixos:/etc/nixos]# mount /dev/disk/by-label/boot /mnt/boot

[root@nixos:/etc/nixos]# swapon /dev/disk/by-label/swap
```

## Generate hardware config
```
[root@nixos:/etc/nixos]# nixos-generate-config --root /mnt
writing /mnt/etc/nixos/hardware-configuration.nix...
writing /mnt/etc/nixos/configuration.nix...

[root@nixos:/etc/nixos]#
```

## Install 
Copy the configuration.nix from the directory of this README and then install:
```
[root@nixos:/etc/nixos]# curl \
https://raw.githubusercontent.com/netcrave/nixos-configs/master/laptop/configuration.nix \
-O /mnt/etc/nixos/

[root@nixos:/etc/nixos]# nixos-install
building the configuration in /mnt/etc/nixos/configuration.nix...
```
