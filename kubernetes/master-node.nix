{ config, pkgs, ... }:
{
  imports = 
  [
      ./hardware-configuration.nix
  ];

  # disable our swap file because kubernetes won't work if it's turned on
  # also swapoff /dev/sda2 before nixos-rebuild
  swapDevices = [];  

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelParams = ["console=ttyS0,115200n8"];
  
  i18n = {
     consoleFont = "Lat2-Terminus16";
     consoleKeyMap = "us";
     defaultLocale = "en_US.UTF-8";
  };
  
  time.timeZone = "UTC";
   environment.systemPackages = with pkgs; [
     wget vim curl emacs git jq ethtool cri-tools socat ebtables
   ];

   programs.bash.enableCompletion = true;

   networking.hostName = "kubernetes-1";
   networking.domain = "yourstruly.sx";
   networking.interfaces.enp1s0.ipv4.addresses = [ { address = "206.125.168.74"; prefixLength = 28; } ];
   networking.defaultGateway = "206.125.168.65";
   networking.nameservers = [ "1.1.1.1" ];
   networking.firewall.allowedTCPPorts = [ 22 ];
   networking.firewall.enable = true;
   networking.firewall.allowPing = true;
   networking.extraHosts =
   ''
      206.125.168.74 kubernetes-1.yourstruly.sx kubernetes-1
      206.125.168.75 kubernetes-2.yourstruly.sx kubernetes-2
      206.125.168.76 kubernetes-3.yourstruly.sx kubernetes-3
      206.125.168.77 kubernetes-4.yourstruly.sx kubernetes-4

   '';   

   users.users.root.openssh.authorizedKeys.keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDhV4X1qsnmSUI5uvGuDIRpehlr9n91iMoHXOik0TO/vU8LECnMBITdlnFjsKwQYLaQIlboUe3nKqu69CKtFXVoqXUTLxYAsghlbrnMSW1/bQgAdM/qbucak7V0gFvLNSfQ8QYsTmeoOO3DbzoJh5ifqh4KnmWbeidjJqKfq/5aRIYTLUhmUHF3UpefQc0nxUBi+m8cj40vJhwCARXkH8cKCIomgxCl+75Tj6oAFuBByOEp+/VWptS6sdGc4Qk33spn4JV/f2Somy/ccGGqsTmpLr4yop7y55qF2OEB/xIRF9cWz74uesBCYYKoJOGo3TNQSMUbhb9cnOQ6EuKURmLDJXm4bvRgLjjN7A04RKJ1ffHyuuMEl919NgrqqorbpZZNF5V3JVxFNyQHifEp2XdCLifFWV04FTvfRnCRG/57dZjkeCTqYskIONFYkRjP37wpbbreZHd1OmAqnrEYtunVinNy4vX0v6flbvmyjfnUOCbJMt12rmm4JA5Lq+JY5BOyDKEX2mcyqgel0zW40edxHOuUXpA4z/8PMrzTWnBj1nWXwt6wbCwakH+A1dKMRvcZPL25D34JdjVZdSP2BvMNgG9GpcWv5XfpUzZUSqe3IDMPR2ATwuq8+FwxSrzMWEP2kA/Y9sgS4tz5yq4w9GZ+9tiF+XOiJlq0k+fYgaqqLQ== erratic@molly"];

   services.openssh.enable = true;
   services.openssh.permitRootLogin = "yes";
   services.openssh.passwordAuthentication = false;
   services.openssh.challengeResponseAuthentication = false;
   services.kubernetes = {
     roles = ["master" "node"];
   };

   system.autoUpgrade.enable = true;
   
   system.autoUpgrade.channel = https://nixos.org/channels/nixos-17.03;
}
